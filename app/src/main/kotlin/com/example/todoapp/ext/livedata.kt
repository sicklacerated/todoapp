package com.example.todoapp.ext

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.todoapp.utils.OneShotEvent

typealias OneShotLiveData<T> = LiveData<OneShotEvent<T>>
typealias MutableOneShotLiveData<T> = MutableLiveData<OneShotEvent<T>>

inline fun <T> LiveData<T>.observeNotNull(o: LifecycleOwner, crossinline observer: (T) -> Unit) {
    observe(o) { value ->
        if (value != null) {
            observer(value)
        }
    }
}

inline fun <T> MutableLiveData<T>.update(updater: (T) -> T) {
    value = value?.let(updater)
}

fun <T> MutableOneShotLiveData<T>.setEvent(event: T) {
    value = OneShotEvent(event)
}

fun <T> MutableOneShotLiveData<T>.postEvent(event: T) {
    postValue(OneShotEvent(event))
}

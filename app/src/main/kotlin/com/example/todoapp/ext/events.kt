package com.example.todoapp.ext

import com.example.todoapp.utils.OneShotEvent

inline fun <T> OneShotEvent<T>.doIfConsumed(action: (T) -> Unit) {
    val value = consume()
    if (value != null) {
        action(value)
    }
}

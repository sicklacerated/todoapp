package com.example.todoapp.ext

fun uuid() = java.util.UUID.randomUUID().toString()

fun <T : CharSequence> T.orPlaceholder(placeholder: T) = if (isBlank()) placeholder else this

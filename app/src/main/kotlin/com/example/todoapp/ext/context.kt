package com.example.todoapp.ext

import android.content.Context
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat

@ColorInt
fun Context.findColor(@ColorRes resId: Int): Int = ContextCompat.getColor(this, resId)

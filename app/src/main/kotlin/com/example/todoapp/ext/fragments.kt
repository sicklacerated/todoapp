package com.example.todoapp.ext

import android.view.View
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.fragment.app.Fragment
import com.example.todoapp.R
import com.google.android.material.snackbar.Snackbar

fun Fragment.showSnackbar(
    message: CharSequence,
    view: View = requireView(),
    duration: Int = Snackbar.LENGTH_SHORT,
    @ColorRes backgroundTint: Int = R.color.primary
) {
    Snackbar.make(view, message, duration)
        .setBackgroundTint(requireContext().findColor(backgroundTint))
        .show()
}

fun Fragment.showErrorSnackbar(
    error: Throwable,
    view: View = requireView()
) {
    showSnackbar(
        view = view,
        message = error.message ?: getString(R.string.wtf),
        backgroundTint = R.color.error
    )
}

fun Fragment.showToast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(requireContext(), message, duration).show()
}

fun Fragment.showErrorToast(throwable: Throwable, duration: Int = Toast.LENGTH_SHORT) {
    showToast(
        message = throwable.message ?: getString(R.string.wtf),
        duration = duration
    )
}

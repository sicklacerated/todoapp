package com.example.todoapp.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.todoapp.presentation.detail.TodoDetailViewModel
import com.example.todoapp.presentation.list.TodoListViewModel

class AppViewModelFactory(
    private val todoListVMCreator: () -> TodoListViewModel,
    private val todoDetailVMCreator: () -> TodoDetailViewModel
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        when (modelClass) {
            TodoListViewModel::class.java -> todoListVMCreator() as T
            TodoDetailViewModel::class.java -> todoDetailVMCreator() as T
            else -> throw IllegalArgumentException("no creator for $modelClass")
        }
}

package com.example.todoapp.di

import com.example.todoapp.domain.usecase.ClearTodoListUseCase
import com.example.todoapp.domain.usecase.GetTodoListAscUseCase
import com.example.todoapp.domain.usecase.GetTodoListDescUseCase
import com.example.todoapp.domain.usecase.InsertTodoUseCase
import com.example.todoapp.domain.usecase.RemoveTodoUseCase

interface UseCaseInjector {
    val getTodoListAscUseCase: GetTodoListAscUseCase
    val getTodoListDescUseCase: GetTodoListDescUseCase
    val clearTodoListUseCase: ClearTodoListUseCase
    val insertTodoUseCase: InsertTodoUseCase
    val removeTodoUseCase: RemoveTodoUseCase
}

class AppUseCaseInjector(private val dataInjector: DataInjector) : UseCaseInjector {
    override val getTodoListAscUseCase: GetTodoListAscUseCase
        get() = GetTodoListAscUseCase(dataInjector.todoRepo)

    override val getTodoListDescUseCase: GetTodoListDescUseCase
        get() = GetTodoListDescUseCase(dataInjector.todoRepo)

    override val clearTodoListUseCase: ClearTodoListUseCase
        get() = ClearTodoListUseCase(dataInjector.todoRepo)

    override val insertTodoUseCase: InsertTodoUseCase
        get() = InsertTodoUseCase(dataInjector.todoRepo)

    override val removeTodoUseCase: RemoveTodoUseCase
        get() = RemoveTodoUseCase(dataInjector.todoRepo)
}

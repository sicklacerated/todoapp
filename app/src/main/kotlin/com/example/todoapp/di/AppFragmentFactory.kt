package com.example.todoapp.di

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.ViewModelProvider
import com.example.todoapp.presentation.detail.TodoDetailFragment
import com.example.todoapp.presentation.list.TodoListFragment

class AppFragmentFactory(
    private val viewModelFactory: ViewModelProvider.Factory
) : FragmentFactory() {
    override fun instantiate(classLoader: ClassLoader, className: String): Fragment =
        when (className) {
            TodoListFragment::class.java.name -> TodoListFragment(viewModelFactory)
            TodoDetailFragment::class.java.name -> TodoDetailFragment(viewModelFactory)
            else -> super.instantiate(classLoader, className)
        }
}

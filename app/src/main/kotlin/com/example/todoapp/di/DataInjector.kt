package com.example.todoapp.di

import android.content.Context
import androidx.preference.PreferenceManager
import com.example.todoapp.data.repo.PrefsTodoRepo
import com.example.todoapp.domain.repo.TodoRepo

interface DataInjector {
    val todoRepo: TodoRepo
}

class AppDataInjector(private val appContext: Context) : DataInjector {
    private val prefs by lazy {
        PreferenceManager.getDefaultSharedPreferences(appContext)
    }

    override val todoRepo: TodoRepo by lazy {
        PrefsTodoRepo(prefs)
    }
}

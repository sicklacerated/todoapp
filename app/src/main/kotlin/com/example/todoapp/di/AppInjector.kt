package com.example.todoapp.di

import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.example.todoapp.utils.LifecycleCallbacks

class AppInjector(appContext: Context) : LifecycleCallbacks() {
    private val dataInjector = AppDataInjector(appContext)
    private val useCaseInjector = AppUseCaseInjector(dataInjector)
    private val viewModelInjector = AppViewModelInjector(useCaseInjector)

    private val fragmentFactory = AppFragmentFactory(viewModelInjector.factory)

    override fun onActivityPreCreated(activity: Activity, savedInstanceState: Bundle?) {
        if (activity is FragmentActivity) {
            activity.supportFragmentManager.registerFragmentLifecycleCallbacks(this, true)
            activity.supportFragmentManager.fragmentFactory = fragmentFactory
        }
    }

    override fun onFragmentPreCreated(
        fm: FragmentManager,
        f: Fragment,
        savedInstanceState: Bundle?
    ) {
        f.childFragmentManager.fragmentFactory = fragmentFactory
    }

    override fun onActivityPostDestroyed(activity: Activity) {
        if (activity is FragmentActivity) {
            activity.supportFragmentManager.unregisterFragmentLifecycleCallbacks(this)
        }
    }
}

package com.example.todoapp.di

import androidx.lifecycle.ViewModelProvider
import com.example.todoapp.presentation.detail.TodoDetailViewModel
import com.example.todoapp.presentation.list.TodoListViewModel

interface ViewModelInjector {
    val factory: ViewModelProvider.Factory
}

class AppViewModelInjector(
    private val useCaseInjector: UseCaseInjector
) : ViewModelInjector {
    override val factory: ViewModelProvider.Factory =
        AppViewModelFactory(
            ::createTodoListViewModel,
            ::createTodoDetailViewModel
        )

    private fun createTodoListViewModel(): TodoListViewModel =
        TodoListViewModel(
            useCaseInjector.getTodoListAscUseCase,
            useCaseInjector.getTodoListDescUseCase,
            useCaseInjector.clearTodoListUseCase,
            useCaseInjector.insertTodoUseCase,
            useCaseInjector.removeTodoUseCase
        )

    private fun createTodoDetailViewModel(): TodoDetailViewModel =
        TodoDetailViewModel(
            useCaseInjector.insertTodoUseCase,
            useCaseInjector.removeTodoUseCase
        )
}

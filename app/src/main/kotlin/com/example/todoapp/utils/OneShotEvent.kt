package com.example.todoapp.utils

class OneShotEvent<T>(private val value: T) {
    private var _isConsumed = false
    val isConsumed get() = _isConsumed

    fun consume(): T? {
        if (isConsumed) {
            return null
        }

        _isConsumed = true
        return value
    }

    fun peek(): T = value
}

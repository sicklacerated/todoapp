package com.example.todoapp.presentation.detail

import android.app.Dialog
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.example.todoapp.R
import com.example.todoapp.domain.entity.Todo
import com.example.todoapp.ext.doIfConsumed
import com.example.todoapp.ext.observeNotNull
import com.example.todoapp.ext.showErrorToast
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.textfield.TextInputLayout

class TodoDetailFragment(factory: ViewModelProvider.Factory) : BottomSheetDialogFragment() {
    private val viewModel by viewModels<TodoDetailViewModel> { factory }

    private var titleLayout: TextInputLayout? = null
    private var titleInputField: EditText? = null
    private var descriptionLayout: TextInputLayout? = null
    private var descriptionInputField: EditText? = null

    private var deleteButton: ImageView? = null
    private var cancelButton: Button? = null
    private var okButton: Button? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)
        dialog.setContentView(R.layout.fragment_todo_detail)
        dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED

        val todo = TodoDetailParams.from(arguments).todo
        viewModel.onCreate(todo)

        titleLayout = dialog.findViewById(R.id.input_layout_title)
        titleInputField = dialog.findViewById(R.id.input_field_title)
        descriptionLayout = dialog.findViewById(R.id.input_layout_description)
        descriptionInputField = dialog.findViewById(R.id.input_field_description)
        deleteButton = dialog.findViewById(R.id.button_delete_todo)
        cancelButton = dialog.findViewById(R.id.button_cancel)
        okButton = dialog.findViewById(R.id.button_ok)

        titleInputField?.doAfterTextChanged {
            viewModel.onTitleChanged(it?.toString() ?: "")
        }

        descriptionInputField?.doAfterTextChanged {
            viewModel.onDescriptionChanged(it?.toString() ?: "")
        }

        cancelButton?.setOnClickListener {
            dismissAllowingStateLoss()
        }

        okButton?.setOnClickListener {
            viewModel.onOkPressed()
        }

        deleteButton?.setOnClickListener {
            viewModel.onRemovePressed()
        }

        return dialog
    }

    override fun onResume() {
        super.onResume()
        observeViewStates()
        observeErrors()
        observeCloseEvents()
    }

    private fun observeViewStates() {
        viewModel.states.observe(this) { state ->
            setLimits(state.titleMaxLength, state.descriptionMaxLength)
            setTodo(state.todo)
            setSaving(state.isSaving)
        }
    }

    private fun setLimits(titleMaxLength: Int, descriptionMaxLength: Int) {
        titleLayout?.counterMaxLength = titleMaxLength
        descriptionLayout?.counterMaxLength = descriptionMaxLength
    }

    private fun setTodo(todo: Todo) {
        if (todo.title.isNotEmpty() && titleInputField?.text.isNullOrEmpty()) {
            titleInputField?.setText(todo.title)
        }

        if (todo.description.isNotEmpty() && descriptionInputField?.text.isNullOrEmpty()) {
            descriptionInputField?.setText(todo.description)
        }
    }

    private fun setSaving(isSaving: Boolean) {
        titleInputField?.isEnabled = !isSaving
        descriptionInputField?.isEnabled = !isSaving
        deleteButton?.isEnabled = !isSaving
        cancelButton?.isEnabled = !isSaving
        okButton?.isEnabled = !isSaving
    }

    private fun observeErrors() {
        viewModel.errors.observeNotNull(this) { event ->
            event.doIfConsumed(::showError)
        }
    }

    private fun showError(throwable: Throwable) {
        showErrorToast(throwable)
    }

    private fun observeCloseEvents() {
        viewModel.closeEvents.observeNotNull(this) { event ->
            event.doIfConsumed { dismissAllowingStateLoss() }
        }
    }

    override fun onPause() {
        removeObservers()
        super.onPause()
    }

    private fun removeObservers() {
        viewModel.states.removeObservers(this)
        viewModel.errors.removeObservers(this)
        viewModel.closeEvents.removeObservers(this)
    }
}

package com.example.todoapp.presentation.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.domain.entity.Todo
import com.example.todoapp.ext.orPlaceholder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_todo.*

class TodoListAdapter(
    private val onCheckboxPressed: (Todo) -> Unit,
    private val onItemPressed: (Todo) -> Unit,
    private val onDeletePressed: (Todo) -> Unit
) : ListAdapter<Todo, TodoListAdapter.ViewHolder>(ITEM_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_todo, parent, false)
        return ViewHolder(v, onCheckboxPressed, onItemPressed, onDeletePressed)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = currentList[position]
        holder.bind(item)
    }

    class ViewHolder(
        override val containerView: View,
        private val onCheckboxPressed: (Todo) -> Unit,
        private val onItemPressed: (Todo) -> Unit,
        private val onDeletePressed: (Todo) -> Unit
    ) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(item: Todo) {
            checkbox.isChecked = item.isDone
            title.text = item.title.orPlaceholder(itemView.context.getString(R.string.no_title))
            description.text = item.description
            checkbox.setOnClickListener {
                onCheckboxPressed(item)
            }
            button_delete_todo.setOnClickListener {
                onDeletePressed(item)
            }
            itemView.setOnClickListener {
                onItemPressed(item)
            }
        }
    }
}

private val ITEM_CALLBACK = object : DiffUtil.ItemCallback<Todo>() {
    override fun areItemsTheSame(oldItem: Todo, newItem: Todo): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Todo, newItem: Todo): Boolean {
        return oldItem == newItem
    }

    override fun getChangePayload(oldItem: Todo, newItem: Todo): Any? = Unit
}

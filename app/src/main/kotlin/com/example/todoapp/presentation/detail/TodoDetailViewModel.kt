package com.example.todoapp.presentation.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todoapp.domain.entity.Todo
import com.example.todoapp.domain.usecase.InsertTodoUseCase
import com.example.todoapp.domain.usecase.RemoveTodoUseCase
import com.example.todoapp.ext.MutableOneShotLiveData
import com.example.todoapp.ext.OneShotLiveData
import com.example.todoapp.ext.setEvent
import com.example.todoapp.ext.update
import kotlinx.coroutines.launch
import timber.log.Timber

class TodoDetailViewModel(
    private val insertTodoUseCase: InsertTodoUseCase,
    private val removeTodoUseCase: RemoveTodoUseCase
) : ViewModel() {
    private val _states = MutableLiveData<TodoDetailViewState>()
    val states: LiveData<TodoDetailViewState> get() = _states

    private val _errors = MutableOneShotLiveData<Throwable>()
    val errors: OneShotLiveData<Throwable> get() = _errors

    private val _closeEvents = MutableOneShotLiveData<Unit>()
    val closeEvents: OneShotLiveData<Unit> get() = _closeEvents

    fun onCreate(todo: Todo) {
        _states.value = TodoDetailViewState(
            todo = todo,
            isSaving = false,
            titleMaxLength = InsertTodoUseCase.MAX_TITLE_LENGTH,
            descriptionMaxLength = InsertTodoUseCase.MAX_DESCRIPTION_LENGTH
        )
    }

    fun onTitleChanged(title: String) {
        _states.update {
            it.copy(todo = it.todo.copy(title = title.trim()))
        }
    }

    fun onDescriptionChanged(description: String) {
        _states.update {
            it.copy(todo = it.todo.copy(description = description.trim()))
        }
    }

    fun onOkPressed() {
        viewModelScope.launch {
            try {
                setSaving(true)
                val todo = states.value?.todo
                insertTodoUseCase.execute(todo)
                _closeEvents.setEvent(Unit)
            } catch (e: Exception) {
                setSaving(false)
                onError(e)
            }
        }
    }

    fun onRemovePressed() {
        viewModelScope.launch {
            try {
                setSaving(true)
                val todo = states.value?.todo
                removeTodoUseCase.execute(todo)
                _closeEvents.setEvent(Unit)
            } catch (e: Exception) {
                setSaving(false)
                onError(e)
            }
        }
    }

    private fun setSaving(isSaving: Boolean) {
        _states.update {
            it.copy(isSaving = isSaving)
        }
    }

    private fun onError(throwable: Throwable) {
        _errors.setEvent(throwable)
        Timber.e(throwable)
    }
}

package com.example.todoapp.presentation.list

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.domain.entity.Todo
import com.example.todoapp.ext.doIfConsumed
import com.example.todoapp.ext.observeNotNull
import com.example.todoapp.ext.showErrorSnackbar
import com.example.todoapp.presentation.base.BaseFragment
import com.example.todoapp.presentation.detail.TodoDetailParams
import kotlinx.android.synthetic.main.fragment_todo_list.*

class TodoListFragment(
    factory: ViewModelProvider.Factory
) : BaseFragment(R.layout.fragment_todo_list, factory) {
    private val viewModel by viewModel<TodoListViewModel>()

    private lateinit var adapter: TodoListAdapter
    private var sortMenuItem: MenuItem? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initMenu()
        initList()

        button_add_todo.setOnClickListener {
            viewModel.onAddTodoPressed()
        }

        observeSort()
        observeTodoList()
        observeNavDirections()
        observeErrors()
    }

    private fun initMenu() {
        toolbar.inflateMenu(R.menu.menu_todo_list)
        sortMenuItem = toolbar.menu.findItem(R.id.menu_sort)

        toolbar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.menu_delete -> viewModel.onDeletePressed()
                R.id.menu_sort -> viewModel.onSortPressed()
            }
            true
        }
    }

    private fun initList() {
        todo_list.layoutManager = LinearLayoutManager(requireContext())
        todo_list.addItemDecoration(DividerItemDecoration(requireContext(), RecyclerView.VERTICAL))

        adapter = TodoListAdapter(
            onCheckboxPressed = { viewModel.onTodoCheckboxPressed(it) },
            onItemPressed = { viewModel.onTodoPressed(it) },
            onDeletePressed = { viewModel.onDeleteTodoPressed(it) }
        )

        todo_list.adapter = adapter
    }

    private fun observeSort() {
        viewModel.sortStates.observeNotNull(viewLifecycleOwner) { state ->
            if (state.isDescending) {
                sortMenuItem?.title = getString(R.string.sort_asc)
                sortMenuItem?.setIcon(R.drawable.ic_sort_asc)
            } else {
                sortMenuItem?.title = getString(R.string.sort_desc)
                sortMenuItem?.setIcon(R.drawable.ic_sort_desc)
            }
        }
    }

    private fun observeTodoList() {
        viewModel.todoListStates.observeNotNull(viewLifecycleOwner) { state ->
            progress_bar.isVisible = state.isLoading
            empty_view.isVisible = state.list.isEmpty() && !state.isLoading
            adapter.submitList(state.list)
        }
    }

    private fun observeNavDirections() {
        viewModel.directions.observeNotNull(viewLifecycleOwner) { event ->
            event.doIfConsumed(::navigateWith)
        }
    }

    private fun navigateWith(todo: Todo) {
        val params = TodoDetailParams(todo)
        findNavController().navigate(R.id.navigate_to_details, params.toBundle())
    }

    private fun observeErrors() {
        viewModel.errors.observeNotNull(viewLifecycleOwner) { event ->
            event.doIfConsumed(::showError)
        }
    }

    private fun showError(throwable: Throwable) {
        showErrorSnackbar(throwable)
    }
}

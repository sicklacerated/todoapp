package com.example.todoapp.presentation.list

import com.example.todoapp.domain.entity.Todo

sealed class TodoListViewState {
    data class Sort(val isDescending: Boolean = false) : TodoListViewState()

    data class TodoList(
        val list: List<Todo> = emptyList(),
        val isLoading: Boolean = false
    ) : TodoListViewState()
}

package com.example.todoapp.presentation.detail

import com.example.todoapp.domain.entity.Todo

data class TodoDetailViewState(
    val todo: Todo,
    val isSaving: Boolean,
    val titleMaxLength: Int,
    val descriptionMaxLength: Int
)

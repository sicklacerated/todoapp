package com.example.todoapp.presentation.detail

import android.os.Bundle
import com.example.todoapp.domain.entity.Todo
import com.example.todoapp.ext.uuid

class TodoDetailParams(val todo: Todo) {
    fun toBundle() = Bundle(5).also {
        it.putString(ID, todo.id)
        it.putString(TITLE, todo.title)
        it.putString(DESCRIPTION, todo.description)
        it.putBoolean(DONE, todo.isDone)
        it.putLong(TIME, todo.creationTime)
    }

    companion object {
        private const val ID = "id"
        private const val TITLE = "title"
        private const val DESCRIPTION = "description"
        private const val DONE = "done"
        private const val TIME = "time"

        fun from(bundle: Bundle?): TodoDetailParams {
            val todo = Todo(
                id = bundle?.getString(ID) ?: uuid(),
                title = bundle?.getString(TITLE) ?: "",
                description = bundle?.getString(DESCRIPTION) ?: "",
                isDone = bundle?.getBoolean(DONE) ?: false,
                creationTime = bundle?.getLong(TIME) ?: System.currentTimeMillis()
            )
            return TodoDetailParams(todo)
        }
    }
}

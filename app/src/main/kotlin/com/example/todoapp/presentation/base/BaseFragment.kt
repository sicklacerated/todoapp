package com.example.todoapp.presentation.base

import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.createViewModelLazy
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

open class BaseFragment(
    @LayoutRes contentLayoutId: Int,
    val factory: ViewModelProvider.Factory
) : Fragment(contentLayoutId) {

    inline fun <reified VM : ViewModel> Fragment.viewModel() =
        createViewModelLazy(VM::class, { viewModelStore }, { factory })

    inline fun <reified VM : ViewModel> Fragment.activityViewModel() =
        createViewModelLazy(VM::class, { requireActivity().viewModelStore }, { factory })
}

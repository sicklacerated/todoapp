package com.example.todoapp.presentation.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todoapp.domain.entity.Todo
import com.example.todoapp.domain.usecase.ClearTodoListUseCase
import com.example.todoapp.domain.usecase.GetTodoListAscUseCase
import com.example.todoapp.domain.usecase.GetTodoListDescUseCase
import com.example.todoapp.domain.usecase.InsertTodoUseCase
import com.example.todoapp.domain.usecase.RemoveTodoUseCase
import com.example.todoapp.ext.MutableOneShotLiveData
import com.example.todoapp.ext.OneShotLiveData
import com.example.todoapp.ext.setEvent
import com.example.todoapp.ext.update
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import timber.log.Timber

class TodoListViewModel(
    private val getTodoListAscUseCase: GetTodoListAscUseCase,
    private val getTodoListDescUseCase: GetTodoListDescUseCase,
    private val clearTodoListUseCase: ClearTodoListUseCase,
    private val insertTodoUseCase: InsertTodoUseCase,
    private val removeTodoUseCase: RemoveTodoUseCase
) : ViewModel() {
    private val _sortStates = MutableLiveData(TodoListViewState.Sort())
    val sortStates: LiveData<TodoListViewState.Sort> get() = _sortStates

    private val _todoListStates = MutableLiveData(TodoListViewState.TodoList(isLoading = true))
    val todoListStates: LiveData<TodoListViewState.TodoList> get() = _todoListStates

    private val _directions = MutableOneShotLiveData<Todo>()
    val directions: OneShotLiveData<Todo> get() = _directions

    private val _errors = MutableOneShotLiveData<Throwable>()
    val errors: OneShotLiveData<Throwable> get() = _errors

    private var getTodoListJob: Job? = null

    init {
        subscribeOnTodoList()
    }

    fun onSortPressed() {
        _sortStates.update {
            it.copy(isDescending = !it.isDescending)
        }
        subscribeOnTodoList()
    }

    private fun subscribeOnTodoList() {
        val sortState = sortStates.value ?: TodoListViewState.Sort()
        subscribeBy(!sortState.isDescending)
    }

    private fun subscribeBy(desc: Boolean) {
        getTodoListJob?.cancel()

        getTodoListJob = viewModelScope.launch {
            getTodoListUseCase(desc).execute()
                .map { it.sortedBy { todo -> todo.isDone } }
                .flowOn(Dispatchers.IO)
                .catch { onError(it) }
                .collect {
                    onTodoListReceived(it)
                }
        }
    }

    private fun getTodoListUseCase(desc: Boolean) =
        if (desc) {
            getTodoListDescUseCase
        } else {
            getTodoListAscUseCase
        }

    private fun onTodoListReceived(list: List<Todo>) {
        _todoListStates.update {
            it.copy(list = list, isLoading = false)
        }
    }

    fun onTodoCheckboxPressed(todo: Todo) {
        viewModelScope.launch {
            try {
                val newTodo = todo.copy(isDone = !todo.isDone)
                insertTodoUseCase.execute(newTodo)
            } catch (e: Exception) {
                onError(e)
            }
        }
    }

    fun onAddTodoPressed() {
        val newTodo = Todo()
        navigateToDetails(newTodo)
    }

    fun onTodoPressed(todo: Todo) {
        navigateToDetails(todo)
    }

    fun onDeleteTodoPressed(todo: Todo) {
        viewModelScope.launch {
            try {
                removeTodoUseCase.execute(todo)
            } catch (e: Exception) {
                onError(e)
            }
        }
    }

    private fun navigateToDetails(todo: Todo) {
        _directions.setEvent(todo)
    }

    fun onDeletePressed() {
        viewModelScope.launch {
            try {
                clearTodoListUseCase.execute()
            } catch (e: Exception) {
                onError(e)
            }
        }
    }

    private fun onError(throwable: Throwable) {
        _errors.setEvent(throwable)
        Timber.e(throwable)
    }
}

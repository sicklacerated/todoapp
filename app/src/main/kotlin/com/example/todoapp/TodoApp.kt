package com.example.todoapp

import android.app.Application
import com.example.todoapp.di.AppInjector
import timber.log.Timber

class TodoApp : Application() {
    private val injector by lazy {
        AppInjector(this)
    }

    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(injector)
        plantTimberTrees()
    }

    private fun plantTimberTrees() {
        Timber.plant(Timber.DebugTree())
    }
}

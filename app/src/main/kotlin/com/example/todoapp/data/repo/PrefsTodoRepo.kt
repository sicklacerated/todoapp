package com.example.todoapp.data.repo

import android.content.SharedPreferences
import androidx.core.content.edit
import com.example.todoapp.domain.entity.Todo
import com.example.todoapp.domain.repo.TodoRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.withContext
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

/**
 * naive usage prefs as persistent storage for lists
 * never do this at production
 */
class PrefsTodoRepo(private val prefs: SharedPreferences) : TodoRepo {
    private val stateFlow = MutableSharedFlow<Unit>()

    override suspend fun getAllOnce(): List<Todo> =
        withContext(Dispatchers.IO) {
            val raw = getRawTodoList()

            if (raw != null) {
                Json.decodeFromString(raw)
            } else {
                emptyList()
            }
        }

    override suspend fun insertOrReplace(todo: Todo) =
        withContext(Dispatchers.IO) {
            val result = getAllOnce().toMutableList()
            result.removeSame(todo)
            result += todo
            insertList(result)
            notifyFlow()
        }

    override suspend fun remove(todo: Todo) =
        withContext(Dispatchers.IO) {
            val result = getAllOnce().toMutableList()
            result.removeSame(todo)
            insertList(result)
            notifyFlow()
        }

    private fun insertList(list: List<Todo>) {
        prefs.edit {
            putString(TODO_LIST_KEY, Json.encodeToString(list))
        }
    }

    private fun MutableList<Todo>.removeSame(todo: Todo) {
        val currentTodo = find { it.id == todo.id }
        if (currentTodo != null) {
            remove(currentTodo)
        }
    }

    override suspend fun clean() =
        withContext(Dispatchers.IO) {
            prefs.edit {
                remove(TODO_LIST_KEY)
            }

            notifyFlow()
        }

    override fun getAllFlow(): Flow<List<Todo>> =
        stateFlow
            .onStart { emit(Unit) }
            .map { getAllOnce() }

    private fun getRawTodoList() = prefs.getString(TODO_LIST_KEY, null)

    private suspend fun notifyFlow() {
        stateFlow.emit(Unit)
    }

    companion object {
        private const val TODO_LIST_KEY = "todo_list"
    }
}

package com.example.todoapp.domain.usecase

import com.example.todoapp.domain.entity.Todo
import com.example.todoapp.domain.repo.TodoRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class InsertTodoUseCase(private val repo: TodoRepo) : UseCase<Todo, Unit> {
    override suspend fun execute(input: Todo?) =
        withContext(Dispatchers.IO) {
            val todo = requireNotNull(input)
            check(todo.title.isNotEmpty()) {
                "Title should not be empty"
            }
            check(todo.title.length <= MAX_TITLE_LENGTH) {
                "Title should not be longer than $MAX_TITLE_LENGTH symbol(s)"
            }
            check(todo.description.isNotEmpty()) {
                "Description should not be empty"
            }
            check(todo.description.length <= MAX_DESCRIPTION_LENGTH) {
                "Description should not be longer than $MAX_DESCRIPTION_LENGTH symbol(s)"
            }
            repo.insertOrReplace(todo)
        }

    companion object {
        const val MAX_TITLE_LENGTH = 50
        const val MAX_DESCRIPTION_LENGTH = 255
    }
}

package com.example.todoapp.domain.usecase

import com.example.todoapp.domain.entity.Todo
import com.example.todoapp.domain.repo.TodoRepo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GetTodoListAscUseCase(private val repo: TodoRepo) : UseCaseFlow<Unit, List<Todo>> {
    override fun execute(input: Unit?): Flow<List<Todo>> =
        repo.getAllFlow().map {
            it.sortedBy { todo -> todo.creationTime }
        }
}

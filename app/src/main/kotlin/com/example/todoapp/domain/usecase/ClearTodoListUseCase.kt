package com.example.todoapp.domain.usecase

import com.example.todoapp.domain.repo.TodoRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ClearTodoListUseCase(private val repo: TodoRepo) : UseCase<Unit, Unit> {
    override suspend fun execute(input: Unit?) =
        withContext(Dispatchers.IO) {
            repo.clean()
        }
}

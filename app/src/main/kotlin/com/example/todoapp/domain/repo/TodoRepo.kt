package com.example.todoapp.domain.repo

import com.example.todoapp.domain.entity.Todo
import kotlinx.coroutines.flow.Flow

interface TodoRepo {
    suspend fun getAllOnce(): List<Todo>
    suspend fun insertOrReplace(todo: Todo)
    suspend fun remove(todo: Todo)
    suspend fun clean()

    fun getAllFlow(): Flow<List<Todo>>
}

package com.example.todoapp.domain.usecase

import com.example.todoapp.domain.entity.Todo
import com.example.todoapp.domain.repo.TodoRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RemoveTodoUseCase(private val repo: TodoRepo) : UseCase<Todo, Unit> {
    override suspend fun execute(input: Todo?) =
        withContext(Dispatchers.IO) {
            val todo = requireNotNull(input)
            repo.remove(todo)
        }
}

package com.example.todoapp.domain.usecase

import kotlinx.coroutines.flow.Flow

interface UseCaseFlow<In, Out> {
    fun execute(input: In? = null): Flow<Out>
}

package com.example.todoapp.domain.entity

import com.example.todoapp.ext.uuid
import kotlinx.serialization.Serializable

@Serializable
data class Todo(
    val id: String = uuid(),
    val title: String = "",
    val description: String = "",
    val creationTime: Long = System.currentTimeMillis(),
    val isDone: Boolean = false
)

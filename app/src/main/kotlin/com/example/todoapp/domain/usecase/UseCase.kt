package com.example.todoapp.domain.usecase

interface UseCase<In, Out> {
    suspend fun execute(input: In? = null): Out
}
